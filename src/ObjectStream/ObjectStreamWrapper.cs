﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using uMod.Common.Serialization;

namespace uMod.Common.IO
{
    public class ObjectStreamWrapper<TReadWrite> : ObjectStreamWrapper<TReadWrite, TReadWrite>
        where TReadWrite : class
    {
        public ObjectStreamWrapper(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null) : base(inStream, outStream, binder, logger)
        {
        }
    }

    public class ObjectStreamWrapper<TRead, TWrite>
        where TRead : class
        where TWrite : class
    {
        private readonly BinaryFormatter _binaryFormatter;
        private readonly Stream _inStream;
        private readonly Stream _outStream;
        private readonly ILogger _logger;

        private bool _run;

        public ObjectStreamWrapper(Stream inStream, Stream outStream, SerializationBinder binder = null, ILogger logger = null)
        {
            _inStream = inStream;
            _outStream = outStream;
            _binaryFormatter = new BinaryFormatter { Binder = binder ?? new TypeBinder(), AssemblyFormat = FormatterAssemblyStyle.Simple };
            _run = true;
            _logger = logger;
        }

        public bool CanRead => _run && _inStream.CanRead;

        public bool CanWrite => _run && _outStream.CanWrite;

        public void Close()
        {
            if (!_run)
            {
                return;
            }

            _run = false;
            try
            {
                _outStream.Close();
            }
            catch (Exception ex)
            {
                _logger?.Report(ex);
            }

            try
            {
                _inStream.Close();
            }
            catch (Exception ex)
            {
                _logger?.Report(ex);
            }
        }

        public TRead ReadObject()
        {
            int len = ReadLength();
            return len == 0 ? null : ReadObject(len);
        }

        #region Private stream readers

        private int ReadLength()
        {
            const int lensize = sizeof(int);
            byte[] lenbuf = new byte[lensize];
            int bytesRead = _inStream.Read(lenbuf, 0, lensize);
            if (bytesRead == 0)
            {
                return 0;
            }

            if (bytesRead != lensize)
            {
                //TODO hack to ignore BOM
                Array.Resize(ref lenbuf, Encoding.UTF8.GetPreamble().Length);
                if (Encoding.UTF8.GetPreamble().SequenceEqual(lenbuf))
                {
                    return ReadLength();
                }

                throw new IOException($"Expected {lensize} bytes but read {bytesRead}");
            }
            return IPAddress.NetworkToHostOrder(BitConverter.ToInt32(lenbuf, 0));
        }

        private TRead ReadObject(int len)
        {
            byte[] data = new byte[len];
            int count;
            int sum = 0;

            while (len - sum > 0 && (count = _inStream.Read(data, sum, len - sum)) > 0)
            {
                sum += count;
            }

            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                try
                {
                    return (TRead)_binaryFormatter.Deserialize(memoryStream);
                }
                catch (Exception exception)
                {
                    _logger?.Report($"Deserialization failed {typeof(TRead).FullName}", exception);
                }
            }

            return null;
        }

        #endregion Private stream readers

        public void WriteObject(TWrite obj)
        {
            byte[] data = Serialize(obj);
            WriteLength(data.Length);
            WriteObject(data);
            Flush();
        }

        #region Private stream writers

        private byte[] Serialize(TWrite obj)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                try
                {
                    _binaryFormatter.Serialize(memoryStream, obj);
                }
                catch (Exception exception)
                {
                    _logger?.Report($"Serialization failure ({typeof(TWrite).FullName})", exception);
                }

                return memoryStream.ToArray();
            }
        }

        private void WriteLength(int len)
        {
            byte[] lenbuf = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(len));
            _outStream.Write(lenbuf, 0, lenbuf.Length);
        }

        private void WriteObject(byte[] data)
        {
            _outStream.Write(data, 0, data.Length);
        }

        private void Flush()
        {
            _outStream.Flush();
        }

        #endregion Private stream writers
    }
}
