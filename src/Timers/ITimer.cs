﻿using System;

namespace uMod.Common
{
    public interface ITimer
    {
        int Repetitions { get; }

        float Delay { get; }

        Action Callback { get; }

        bool Destroyed { get; }

        IPlugin Owner { get; }
    }
}
