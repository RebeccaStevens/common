﻿using System.Collections.Generic;

namespace uMod.Common
{
    /// <summary>
    /// Filters for finding players by state or status
    /// </summary>
    public enum PlayerFilter
    {
        All = 0,
        Connected = 1,
        Admin = 2,
        Moderator = 4,
        Banned = 8,
        Alive = 16,
        Dead = 32,
        Sleeping = 64,
        Id = 128,
        Name = 256,
        Address = 512
    }

    /// <summary>
    /// Represents a player manager
    /// </summary>
    public interface IPlayerManager : ISingleton
    {
        #region Player Finding

        /// <summary>
        /// Gets all players
        /// </summary>
        /// <returns></returns>
        IEnumerable<IPlayer> All { get; }

        /// <summary>
        /// Gets all connected players
        /// </summary>
        /// <returns></returns>
        IEnumerable<IPlayer> Connected { get; }

        /// <summary>
        /// Gets all sleeping players
        /// </summary>
        /// <returns></returns>
        IEnumerable<IPlayer> Sleeping { get; }

        /// <summary>
        /// Finds a single player given unique ID
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        IPlayer FindPlayerById(string playerId);

        /// <summary>
        /// Finds a single connected player given game object
        /// </summary>
        /// <param name="playerObj"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IPlayer FindPlayerByObj(object playerObj, int playerFilter = 1);

        /// <summary>
        /// Finds a single connected player given game object
        /// </summary>
        /// <param name="playerObj"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IPlayer FindPlayerByObj(object playerObj, PlayerFilter playerFilter);

        /// <summary>
        /// Finds a single player given a partial name or unique ID (case-insensitive, wildcards accepted, multiple matches returns null)
        /// </summary>
        /// <param name="partialNameOrId"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IPlayer FindPlayer(string partialNameOrId, int playerFilter = 0);

        /// <summary>
        /// Finds a single player given a partial name or unique ID (case-insensitive, wildcards accepted, multiple matches returns null)
        /// </summary>
        /// <param name="partialNameOrId"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IPlayer FindPlayer(string partialNameOrId, PlayerFilter playerFilter);

        /// <summary>
        /// Finds any number of players given a partial name or unique ID (case-insensitive, wildcards accepted)
        /// </summary>
        /// <param name="partialNameOrId"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IEnumerable<IPlayer> FindPlayers(string partialNameOrId, int playerFilter = 0);

        /// <summary>
        /// Finds any number of players given a partial name or unique ID (case-insensitive, wildcards accepted)
        /// </summary>
        /// <param name="partialNameOrId"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IEnumerable<IPlayer> FindPlayers(string partialNameOrId, PlayerFilter playerFilter);

        /// <summary>
        /// Finds multiple players that match the given filter
        /// </summary>
        /// <param name="partialNameOrId"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IEnumerable<IPlayer> FindPlayers(int playerFilter = 0);

        /// <summary>
        /// Finds multiple players that match the given filter
        /// </summary>
        /// <param name="partialNameOrId"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        IEnumerable<IPlayer> FindPlayers(PlayerFilter playerFilter);

        #endregion Player Finding

        #region Player Creation

        /// <summary>
        /// Creates or updates implied IPlayer instance when a player joins
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        void PlayerJoin(string playerId, string playerName);

        /// <summary>
        /// Creates a new bound IPlayer instance and sets player as connected
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="playerId"></param>
        /// <param name="gamePlayer"></param>
        void PlayerConnected<T>(string playerId, T gamePlayer);

        /// <summary>
        /// Removes player from connected player list on disconnection
        /// </summary>
        /// <param name="playerId"></param>
        void PlayerDisconnected(string playerId);

        #endregion Player Creation
    }
}
