﻿using System;
using System.Globalization;

namespace uMod.Common
{
    /// <summary>
    /// Represents a generic player within a game, either connected or not
    /// </summary>
    public interface IPlayer : IAuthorizable, IIdentity
    {
        /// <summary>
        /// Reconnects the gamePlayer to game object
        /// </summary>
        /// <param name="gamePlayer"></param>
        void Reconnect(object gamePlayer);

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        object Object { get; set; }

        /// <summary>
        /// Gets the player's last used command type
        /// </summary>
        CommandType LastCommand { get; set; }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets the player's IP address
        /// </summary>
        string Address { get; }

        /// <summary>
        /// Gets the player's average network ping
        /// </summary>
        int Ping { get; }

        /// <summary>
        /// Gets the player's language
        /// </summary>
        CultureInfo Language { get; }

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        bool IsBanned { get; }

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        bool IsAlive { get; }

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        bool IsDead { get; }

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        bool IsSleeping { get; }

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        bool IsServer { get; }

        /// <summary>
        /// Gets if the player has connected before
        /// </summary>
        bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets if the player has spawned before
        /// </summary>
        bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        void Ban(string reason = "", TimeSpan duration = default);

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        TimeSpan BanTimeRemaining { get; }

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        void Kick(string reason = "");

        /// <summary>
        /// Unbans the player
        /// </summary>
        void Unban();

        #endregion Administration

        #region Character

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        void Heal(float amount);

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        float Health { get; set; }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        float MaxHealth { get; set; }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        void Hurt(float amount);

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        void Kill();

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        void Rename(string newName);

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        void Reset();

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        void Respawn();

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        void Respawn(Position pos);

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of this character
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void Position(out float x, out float y, out float z);

        /// <summary>
        /// Gets the position of this character
        /// </summary>
        /// <returns></returns>
        Position Position();

        /// <summary>
        /// Teleports the player's character to the specified coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void Teleport(float x, float y, float z);

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        void Teleport(Position pos);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        void Message(string message, string prefix, params object[] args);

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        void Message(string message);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        void Reply(string message, string prefix, params object[] args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        void Reply(string message);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        void Command(string command, params object[] args);

        #endregion Chat and Commands
    }
}
