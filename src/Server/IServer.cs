﻿using System;
using System.Globalization;
using System.Net;

namespace uMod.Common
{
    /// <summary>
    /// Represents a generic server hosting the game instance
    /// </summary>
    public interface IServer : INameable, ISingleton
    {
        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        new string Name { get; set; }

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        IPAddress Address { get; }

        /// <summary>
        /// Gets the internal IP address of the server, if known
        /// </summary>
        IPAddress LocalAddress { get; }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        ushort Port { get; }

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        string Protocol { get; }

        /// <summary>
        /// Gets the language for the server
        /// </summary>
        CultureInfo Language { get; }

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        int Players { get; }

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        int MaxPlayers { get; set; }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        DateTime Time { get; set; }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        int FrameRate { get; }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        int TargetFrameRate { get; set; }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo SaveInfo { get; }

        /// <summary>
        /// Gets or sets player manager
        /// </summary>
        IPlayerManager PlayerManager { get; set; }

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        void Save();

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        void Shutdown(bool save = true, int delay = 0);

        #endregion Server Administration

        #region Player Administration

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        bool IsBanned(string playerId);

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        void Ban(string playerId, string reason = "", TimeSpan duration = default);

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        TimeSpan BanTimeRemaining(string playerId);

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        void Unban(string playerId);

        #endregion Player Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        void Broadcast(string message, string prefix, params object[] args);

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        void Broadcast(string message, string prefix, ulong id, params object[] args);

        /// <summary>
        /// Broadcasts the specified chat message to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        void Broadcast(string message, ulong id, params object[] args);

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        void Broadcast(string message);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        void Command(string command, params object[] args);

        #endregion Chat and Commands
    }
}
