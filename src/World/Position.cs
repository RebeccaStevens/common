﻿using System;
#pragma warning disable 618

namespace uMod.Common
{
    /// <summary>
    /// Represents a vector position in 3D space
    /// </summary>
    [Obsolete("Use uMod.Common.Position instead")]
    public class GenericPosition : Point
    {
        public float Z;

        /// <summary>
        /// Create position object
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public GenericPosition(float x, float y, float z) : base(x, y)
        {
            Z = z;
        }

        /// <summary>
        /// Create empty position object
        /// </summary>
        public GenericPosition()
        {
        }

        /// <summary>
        /// Try to parse a string as position
        /// </summary>
        /// <param name="value"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out GenericPosition point)
        {
            point = null;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            value = value.Replace(",", "");
            value = value.Replace("(", "");
            value = value.Replace(")", "");
            if (value.IndexOf(' ') <= -1)
            {
                return false;
            }

            string[] parts = value.Split(' ');
            if (parts.Length < 3)
            {
                return false;
            }

            if (!float.TryParse(parts[0], out float x))
            {
                return false;
            }

            if (!float.TryParse(parts[1], out float y))
            {
                return false;
            }

            if (!float.TryParse(parts[2], out float z))
            {
                return false;
            }

            point = new GenericPosition(x, y, z);
            return true;
        }

        /// <summary>
        /// Determine equality of specified object to Position
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is GenericPosition pos)
            {
                return X.Equals(pos.X) && Y.Equals(pos.Y) && Z.Equals(pos.Z);
            }

            return false;
        }

        /// <summary>
        /// Position equals operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(GenericPosition a, GenericPosition b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if ((object)a == null || (object)b == null)
            {
                return false;
            }

            return a.X.Equals(b.X) && a.Y.Equals(b.Y) && a.Z.Equals(b.Z);
        }

        /// <summary>
        /// Position not equals operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(GenericPosition a, GenericPosition b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Position addition operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static GenericPosition operator +(GenericPosition a, GenericPosition b)
        {
            return new GenericPosition(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        /// <summary>
        /// Position subtraction operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static GenericPosition operator -(Position a, GenericPosition b)
        {
            return new GenericPosition(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        /// <summary>
        /// Position multiplication operator
        /// </summary>
        /// <param name="mult"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static GenericPosition operator *(float mult, GenericPosition a)
        {
            return new GenericPosition(a.X * mult, a.Y * mult, a.Z * mult);
        }

        /// <summary>
        /// Position multiplication operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="mult"></param>
        /// <returns></returns>
        public static GenericPosition operator *(GenericPosition a, float mult)
        {
            return new GenericPosition(a.X * mult, a.Y * mult, a.Z * mult);
        }

        /// <summary>
        /// Position division operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="div"></param>
        /// <returns></returns>
        public static GenericPosition operator /(GenericPosition a, float div)
        {
            return new GenericPosition(a.X / div, a.Y / div, a.Z / div);
        }

        /// <summary>
        /// Get Position HashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => new { X, Y, Z }.GetHashCode();

        /// <summary>
        /// Get string representation of Position
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"({X}, {Y}, {Z})";

        /// <summary>
        /// Generic empty Position
        /// </summary>
        public new static GenericPosition Empty { get; } = new GenericPosition();
    }

    /// <summary>
    /// Represents a vector position in 3D space
    /// </summary>
    public class Position : GenericPosition
    {
        /// <summary>
        /// Create empty position object
        /// </summary>
        public Position()
        {
        }

        /// <summary>
        /// Create position object
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Position(float x, float y, float z) : base(x, y, z)
        {
            
        }

        /// <summary>
        /// Try to parse a string as position
        /// </summary>
        /// <param name="value"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out Position point)
        {
            point = null;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            value = value.Replace(",", "");
            value = value.Replace("(", "");
            value = value.Replace(")", "");
            if (value.IndexOf(' ') <= -1)
            {
                return false;
            }

            string[] parts = value.Split(' ');
            if (parts.Length < 3)
            {
                return false;
            }

            if (!float.TryParse(parts[0], out float x))
            {
                return false;
            }

            if (!float.TryParse(parts[1], out float y))
            {
                return false;
            }

            if (!float.TryParse(parts[2], out float z))
            {
                return false;
            }

            point = new Position(x, y, z);
            return true;
        }

        public new static Position Empty { get; } = new Position();
    }
}
