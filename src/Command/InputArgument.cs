﻿using System;

namespace uMod.Common.Command
{
    /// <summary>
    /// Types of input arguments
    /// </summary>
    [Flags]
    public enum InputArgumentType
    {
        Optional = 0,
        Required = 1,
        Array = 2
    }

    /// <summary>
    /// Represents an input argument
    /// </summary>
    public struct InputArgument : INameable
    {
        /// <summary>
        /// Argument name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Argument type
        /// </summary>
        public readonly InputArgumentType Type;

        /// <summary>
        /// Argument description
        /// TODO: Implement this
        /// </summary>
        public readonly string Description;

        /// <summary>
        /// Argument default
        /// </summary>
        public readonly object Default;

        /// <summary>
        /// Determine if argument is optional
        /// </summary>
        public bool IsOptional => !IsRequired;

        /// <summary>
        /// Determine if argument is required
        /// </summary>
        public bool IsRequired => (Type & InputArgumentType.Required) != 0;

        /// <summary>
        /// Determine if argument is array
        /// </summary>
        public bool IsArray => (Type & InputArgumentType.Array) != 0;

        /// <summary>
        /// Creates a new input argument struct
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="description"></param>
        /// <param name="default"></param>
        public InputArgument(string name, InputArgumentType type = InputArgumentType.Optional, string description = null, object @default = null)
        {
            Name = name;
            Type = type;
            Description = description;
            Default = @default;
        }
    }
}
