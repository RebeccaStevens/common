﻿using System;
using System.Collections.Generic;

namespace uMod.Common.Command
{
    /// <summary>
    /// Represents a command argument container
    /// </summary>
    public interface IArgs : IDisposable, IEnumerable<object>
    {
        /// <summary>
        /// Gets the command verb
        /// </summary>
        string Command { get; }

        /// <summary>
        /// Gets an enumerable representation of the command arguments
        /// </summary>
        IEnumerable<CommandArgument> Arguments { get; }

        /// <summary>
        /// Gets extra objects attached to arguments
        /// </summary>
        object[] ContextObjects { get; }

        /// <summary>
        /// Gets the argument keys
        /// </summary>
        IEnumerable<string> Keys { get; }

        /// <summary>
        /// Gets the argument values
        /// </summary>
        IEnumerable<object> Values { get; }

        /// <summary>
        /// Gets the number of arguments
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Gets the command definition
        /// </summary>
        ICommandDefinition Definition { get; }

        /// <summary>
        /// Gets the player context
        /// </summary>
        IPlayer Player { get; }

        /// <summary>
        /// Determine if player context is a server
        /// </summary>
        bool IsServer { get; }

        /// <summary>
        /// Determines if player context is an administrator
        /// </summary>
        bool IsAdmin { get; }

        /// <summary>
        /// Determines if player context is a moderator
        /// </summary>
        bool IsModerator { get; }

        /// <summary>
        /// Determine if required arguments are provided
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// Gets an argument value by name
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object this[string key] { get; }

        /// <summary>
        /// Gets an argument string representation by index
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string this[int key] { get; }

        /// <summary>
        /// Reset Args representation after pooling
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="player"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        IArgs Reset(ICommandDefinition definition, IPlayer player, string command, object[] context = null);

        /// <summary>
        /// Determines whether argument exists by name
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool HasArgument(string key);

        /// <summary>
        /// Determine if argument is of type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        bool IsArgument(string key, Type type);

        /// <summary>
        /// Determine if argument is of type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        bool IsArgument<T>(string key);

        /// <summary>
        /// Determine if argument is default
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool IsDefault(string key);

        /// <summary>
        /// Try to get a context object of the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool TryGetContext(Type type, out object value);

        /// <summary>
        /// Try to get a context object of the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool TryGetContext<T>(Type type, out T value);

        /// <summary>
        /// Try to get an argument by name
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool TryGetArgument(string key, out object value);

        /// <summary>
        /// Try to get an argument by name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool TryGetArgument<T>(string key, out T value);

        /// <summary>
        /// Gets an argument value by name with optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        object GetArgument(string key, object @default = null);

        /// <summary>
        /// Gets a generic argument value by name with an optional default result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        T GetArgument<T>(string key, T @default = default);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        string GetString(string key, string @default = default);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        bool GetBool(string key, bool @default = default);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        bool GetBoolean(string key, bool @default = default);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        float GetFloat(string key, float @default = 0.0f);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        double GetDouble(string key, double @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        decimal GetDecimal(string key, decimal @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        short GetShort(string key, short @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        ushort GetUShort(string key, ushort @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        byte GetByte(string key, byte @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        sbyte GetSByte(string key, sbyte @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        int GetInteger(string key, int @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        int GetInt(string key, int @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        uint GetUInt(string key, uint @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        long GetInt64(string key, long @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        long GetLong(string key, long @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        ulong GetUInt64(string key, ulong @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        ulong GetULong(string key, ulong @default = 0);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        TimeSpan GetTimeSpan(string key);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        DateTime GetDateTime(string key);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        Position GetPosition(string key, Position @default = null);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        Position4 GetPosition4(string key, Position4 @default = null);

        /// <summary>
        /// Gets an argument value by name with an optional default result
        /// </summary>
        /// <param name="key"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        Point GetPoint(string key, Point @default = null);

        /// <summary>
        /// Reply to player context with specified message
        /// </summary>
        /// <param name="message"></param>
        void Reply(string message);

        /// <summary>
        /// Reply to player context with specified message (with optional format parameters and prefix)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        void Reply(string message, string prefix, params object[] args);

        /// <summary>
        /// Send message to player context with specified message
        /// </summary>
        /// <param name="message"></param>
        void Message(string message);

        /// <summary>
        /// Send message to player context with specified message (with optional format parameters and prefix)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        void Message(string message, string prefix, params object[] args);

        /// <summary>
        /// Shift an argument off the beginning of the args
        /// </summary>
        /// <returns></returns>
        IArgs Shift(ICommandDefinition definition = null);
    }
}
