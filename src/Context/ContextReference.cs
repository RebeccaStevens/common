﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Context reference which stores reference to context
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ContextReference<T> : IDisposable, IEventContext where T : class, IContext
    {
        private T context;
        private bool disposed = false;

        /// <summary>
        /// Get referenced context
        /// </summary>
        public IContext Context
        {
            get
            {
                return context;
            }
            private set
            {
                context = (T)value;
            }
        }

        /// <summary>
        /// Get context name
        /// </summary>
        public string Name => context.Name;

        /// <summary>
        /// Create context reference object
        /// </summary>
        public ContextReference() { }

        /// <summary>
        /// Create context reference object with specified context
        /// </summary>
        /// <param name="context"></param>
        public ContextReference(T context) : base()
        {
            this.context = context;
        }

        /// <summary>
        /// Create context reference object with specified context
        /// </summary>
        /// <param name="context"></param>
        public ContextReference(IContext context) : base()
        {
            Context = context;
        }

        /// <summary>
        /// Reset context reference
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public ContextReference<T> Reset(T context)
        {
            this.context = context;
            disposed = false;
            return this;
        }

        /// <summary>
        /// Reset context reference
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public ContextReference<T> Reset(IContext context)
        {
            Context = context;
            disposed = false;
            return this;
        }

        /// <summary>
        /// Dispose of context reference
        /// </summary>
        public void Dispose()
        {
            if (!disposed)
            {
                context = null;
                disposed = true;
            }
        }
    }
}
