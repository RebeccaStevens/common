﻿namespace uMod.Common
{
    /// <summary>
    /// Nameable interface
    /// </summary>
    public interface INameable
    {
        /// <summary>
        /// Gets the name
        /// </summary>
        string Name { get; }
    }
}
