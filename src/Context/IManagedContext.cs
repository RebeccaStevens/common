﻿namespace uMod.Common
{
    /// <summary>
    /// Generic context manager event
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TManager"></typeparam>
    public interface IContextManagerEvent<TContext, TManager> : IEvent<TContext, TManager> where TContext : IContext where TManager : IContextManager<TContext>
    {
    }

    /// <summary>
    /// Managed context interface
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TManager"></typeparam>
    public interface IManagedContext<TContext, TManager> : INameable where TContext : IContext where TManager : IContextManager<TContext>
    {
        IContextManagerEvent<TContext, TManager> OnAddedToManager { get; }
        IContextManagerEvent<TContext, TManager> OnRemovedFromManager { get; }

        /// <summary>
        /// Invoked when context added to manager
        /// </summary>
        /// <param name="manager"></param>
        void HandleAddedToManager(TManager manager);

        /// <summary>
        /// Invoked when context removed from manager
        /// </summary>
        /// <param name="manager"></param>
        void HandleRemovedFromManager(TManager manager);
    }
}
