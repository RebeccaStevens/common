﻿using System;

namespace uMod.Common
{
    public interface IDispatcher
    {
        void Stop();
    }

    public interface IChainDispatcher : IDispatcher
    {
        void Dispatch(Func<bool> callback, ref object scopeLock);

        void Dispatch(Func<bool> callback);
    }

    public interface IHookDispatcher : IDispatcher
    {
        void Dispatch(Action<object, object[]> callback, ref object scopeLock);

        void Dispatch(Func<object, object[], object> callback, ref object scopeLock);
    }
}
