﻿using System;
using System.Threading;

namespace uMod.Common.Threading
{
    public class Worker
    {
        public event WorkerExceptionEventHandler Error;

        public void DoWork(Action action)
        {
            ThreadPool.QueueUserWorkItem(DoWorkImpl, action);
        }

        private void DoWorkImpl(object oAction)
        {
            Action action = (Action)oAction;
            try
            {
                action();
            }
            catch (Exception e)
            {
                Callback(() => Fail(e));
            }
        }

        private void Fail(Exception exception)
        {
            Error?.Invoke(exception);
        }

        private void Callback(Action action)
        {
            ThreadPool.QueueUserWorkItem(DoWorkImpl, action);
        }
    }

    public delegate void WorkerExceptionEventHandler(Exception exception);
}
