﻿namespace uMod.Common
{
    public interface IResource : ISingleton, IContext
    {
        string Title { get; }
        string Author { get; }
        VersionNumber Version { get; }
        string Filename { get; }
    }
}
