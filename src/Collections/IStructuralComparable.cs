namespace System.Collections
{
#if NET35

    public interface IStructuralComparable
    {
        Int32 CompareTo(Object other, IComparer comparer);
    }

#endif
}
