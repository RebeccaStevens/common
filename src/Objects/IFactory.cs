﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Factory interface
    /// </summary>
    public interface IFactory
    {
        TResult MakeParams<TResult>(params object[] arguments);

        TResult MakeParams<TResult>(Type type, params object[] arguments);

        TResult Make<TResult>(object[] arguments = null);

        TResult Make<TResult>(Type type, object[] arguments = null);

        TResult Make<TResult, TArg1>(TArg1 arg);

        TResult Make<TResult, TArg1, TArg2>(TArg1 arg, TArg2 arg2);

        TResult Make<TResult, TArg1, TArg2, TArg3>(TArg1 arg, TArg2 arg2, TArg3 arg3);

        TResult Make<TResult, TArg1, TArg2, TArg3, TArg4>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        TResult Make<TResult, TArg1, TArg2, TArg3, TArg4, TArg5>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

        TResult Make<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6);
    }

    /// <summary>
    /// Factory interface with generic type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IFactory<T> : IFactory
    {
        T Make(object[] arguments = null);

        T Make<TArg1>(TArg1 arg);

        T Make<TArg1, TArg2>(TArg1 arg, TArg2 arg2);

        T Make<TArg1, TArg2, TArg3>(TArg1 arg, TArg2 arg2, TArg3 arg3);

        T Make<TArg1, TArg2, TArg3, TArg4>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        T Make<TArg1, TArg2, TArg3, TArg4, TArg5>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

        T Make<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6);
    }
}
