﻿using System;

namespace uMod.Common.Database
{
    /// <summary>
    /// Specify field or property as key
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KeyAttribute : Attribute
    {
    }

    /// <summary>
    /// Specify field or property as primary key
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public sealed class PrimaryKeyAttribute : KeyAttribute
    {
    }

    /// <summary>
    /// Specify field or property as a one to one relationship
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public sealed class OneToOneAttribute : Attribute
    {
    }

    /// <summary>
    /// Specify field or property as a one to many relationship
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public sealed class OneToManyAttribute : Attribute
    {
    }
}
