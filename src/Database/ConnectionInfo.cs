﻿using System;

namespace uMod.Common.Database
{
    [Serializable]
    public enum ConnectionType
    {
        None,
        MySQL,
        SQLite
    }

    [Serializable]
    public enum ConnectionState
    {
        Broken,
        Closed,
        Connecting,
        Executing,
        Fetching,
        Open
    }

    [Serializable]
    public class ConnectionInfo
    {
        public string Name;
        public ConnectionType Type;
        public string ConnectionString;
        public bool Persistent;

        public override string ToString()
        {
            return $"{Type} Connection: {Name}";
        }
    }
}
