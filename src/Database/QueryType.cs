﻿using System;

namespace uMod.Common.Database
{
    [Serializable]
    public enum QueryType
    {
        Query,
        Execute,
        ExecuteScalar,
        QueryFirst,
        QuerySingle
    }
}
