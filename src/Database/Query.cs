﻿using System;

namespace uMod.Common.Database
{
    [Serializable]
    public enum CommandType
    {
        StoredProcedure,
        TableDirect,
        Text
    }

    [Serializable]
    public sealed class Query
    {
        public string Connection;
        public QueryType QueryType;
        public string TypeName;
        public string Sql;
        public object Parameters = null;
        public string Transaction;
        public bool Buffered = true;
        public int Timeout;
        public CommandType CommandType;
        public ObjectModel Model;

        public string SplitOn = "Id";
        public string[] Mapping;

        public override string ToString()
        {
            return Sql;
        }
    }
}
