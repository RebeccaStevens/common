﻿namespace uMod.Common.Database
{
    public interface IQuery
    {
        string Connection { get; }
        string TypeName { get; }
        QueryType QueryType { get; }
        string Sql { get; }
        object Parameters { get; }
        string Transaction { get; }
        bool Buffered { get; }
        int Timeout { get; }
        CommandType CommandType { get; }
        ObjectModel Model { get; }

        string SplitOn { get; }
        string[] Mapping { get; }
    }
}
