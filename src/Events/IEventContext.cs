﻿namespace uMod.Common
{
    /// <summary>
    /// Event context
    /// Used to differentiate special event hooks in some cases
    /// </summary>
    public interface IEventContext : INestedContext
    {
    }
}
