﻿using System;

namespace uMod.Common.Event
{
    public delegate void Action<in T1, in T2, in T3, in T4, in T5>(T1 arg0, T2 arg1, T3 arg2, T4 arg3, T5 arg4);
}

namespace uMod.Common
{
    public interface IEvent
    {
        void Add(ICallback callback);

        ICallback Add(Action callback);

        void Invoke();
    }

    public interface IEvent<T>
    {
        void Add(ICallback<T> callback);

        ICallback<T> Add(Action<T> callback);

        void Invoke();

        void Invoke(T arg0);
    }

    public interface IEvent<T1, T2>
    {
        void Add(ICallback<T1, T2> callback);

        ICallback<T1, T2> Add(Action<T1, T2> callback);

        void Invoke();

        void Invoke(T1 arg0);

        void Invoke(T1 arg0, T2 arg1);
    }

    public interface IEvent<T1, T2, T3>
    {
        void Add(ICallback<T1, T2, T3> callback);

        ICallback<T1, T2, T3> Add(Action<T1, T2, T3> callback);

        void Invoke();

        void Invoke(T1 arg0);

        void Invoke(T1 arg0, T2 arg1);

        void Invoke(T1 arg0, T2 arg1, T3 arg2);
    }

    public interface IEvent<T1, T2, T3, T4>
    {
        void Add(ICallback<T1, T2, T3, T4> callback);

        ICallback<T1, T2, T3, T4> Add(Action<T1, T2, T3, T4> callback);

        void Invoke();

        void Invoke(T1 arg0);

        void Invoke(T1 arg0, T2 arg1);

        void Invoke(T1 arg0, T2 arg1, T3 arg2);

        void Invoke(T1 arg0, T2 arg1, T3 arg2, T4 arg3);
    }

    public interface IEvent<T1, T2, T3, T4, T5>
    {
        void Add(ICallback<T1, T2, T3, T4, T5> callback);

        ICallback<T1, T2, T3, T4, T5> Add(Event.Action<T1, T2, T3, T4, T5> callback);

        void Invoke();

        void Invoke(T1 arg0);

        void Invoke(T1 arg0, T2 arg1);

        void Invoke(T1 arg0, T2 arg1, T3 arg2);

        void Invoke(T1 arg0, T2 arg1, T3 arg2, T4 arg3);

        void Invoke(T1 arg0, T2 arg1, T3 arg2, T4 arg3, T5 arg4);
    }
}
