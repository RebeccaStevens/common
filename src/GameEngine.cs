﻿namespace uMod.Common
{
    /// <summary>
    /// Game engines that are supported
    /// </summary>
    public enum GameEngine
    {
        None,
        Unity,
        XNA,
        Unreal,
        XRAGE
    }
}
