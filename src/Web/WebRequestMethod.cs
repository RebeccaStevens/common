﻿using System;

namespace uMod.Common.Web
{
    /// <summary>
    /// Web request methods
    /// </summary>
    [Serializable]
    public enum WebRequestMethod
    {
        DELETE,
        GET,
        HEAD,
        PATCH,
        POST,
        PUT
    }
}
