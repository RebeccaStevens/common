﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace uMod.Common.Web
{
    /// <summary>
    /// Web request data
    /// </summary>
    [Serializable]
    public sealed class WebRequest
    {
        /// <summary>
        /// Plugin which initiated web request
        /// </summary>
        public string Plugin;

        /// <summary>
        /// Url of web request
        /// </summary>
        public string Url;

        /// <summary>
        /// Web request method (GET, POST, etc)
        /// </summary>
        public WebRequestMethod Method;

        /// <summary>
        /// Maximum duration allowed for request (in seconds)
        /// </summary>
        public int Timeout;

        /// <summary>
        /// Request headers
        /// </summary>
        public IDictionary<string, string> Headers;

        /// <summary>
        /// Request body (POST, PUT, etc)
        /// </summary>
        public string Body;

        /// <summary>
        /// Decompression method
        /// </summary>
        public DecompressionMethods Decompression;

        /// <summary>
        /// Local address
        /// </summary>
        public string Address;

        /// <summary>
        /// Request cookies
        /// </summary>
        public IDictionary<string, string> Cookies;

        /// <summary>
        /// Request files
        /// </summary>
        public IDictionary<string, IMultipartFile> Files;

        /// <summary>
        /// Request form
        /// </summary>
        public IDictionary<string, FormSection> Form;
    }

    public interface IMultipartSection
    {
        byte[] Data { get; }
    }

    public interface IMultipartFile : IMultipartSection
    {
        string FileName { get; }
        string ContentType { get; }
    }

    /// <summary>
    /// Form section
    /// </summary>
    [Serializable]
    public class FormSection : IMultipartSection
    {
        /// <summary>
        /// Form data
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Create a new instance of the FormSection class
        /// </summary>
        public FormSection()
        {
        }

        /// <summary>
        /// Create a new instance of the FormSection class
        /// </summary>
        /// <param name="data"></param>
        public FormSection(byte[] data)
        {
            Data = data;
        }

        /// <summary>
        /// Create a new instance of the FormSection class
        /// </summary>
        /// <param name="data"></param>
        public FormSection(string data)
        {
            Data = Encoding.UTF8.GetBytes(data);
        }

        /// <summary>
        /// Converts form data to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Data == null ? string.Empty : Encoding.UTF8.GetString(Data);
        }

        /// <summary>
        /// Implictly casts a string to a form section
        /// </summary>
        /// <param name="input"></param>
        public static implicit operator FormSection(string input)
        {
            return new FormSection(input);
        }
    }

    /// <summary>
    /// File attachment
    /// </summary>
    [Serializable]
    public class FileAttachment : IMultipartFile
    {
        /// <summary>
        /// File name
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// File content type
        /// </summary>
        public string ContentType { get; set; } = "text/plain";

        /// <summary>
        /// File data
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Create a new instance of the FileAttachment class
        /// </summary>
        public FileAttachment()
        {
        }

        /// <summary>
        /// Create a new instance of the FileAttachment class
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <param name="contentType"></param>
        public FileAttachment(string fileName, byte[] data, string contentType = "text/plain")
        {
            FileName = fileName;
            Data = data;
            ContentType = contentType;
        }

        /// <summary>
        /// Create a new instance of the FileAttachment class
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <param name="contentType"></param>
        public FileAttachment(string fileName, string data, string contentType = "text/plain")
        {
            FileName = fileName;
            Data = Encoding.UTF8.GetBytes(data);
            ContentType = contentType;
        }
    }
}
