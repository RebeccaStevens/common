﻿using System;

namespace uMod.Common.Web
{
    /// <summary>
    /// Web message type
    /// </summary>
    [Serializable]
    public enum WebMessageType
    {
        Request,
        Response,
        Error,
        Exit,
        Ready
    }
}
