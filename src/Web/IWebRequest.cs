﻿using System;
using System.Collections.Generic;

namespace uMod.Common.Web
{
    public interface IWebRequest : IDisposable
    {
        float Timeout { get; }
        WebRequestMethod Method { get; }
        string Url { get; }
        string Body { get; }
        IDictionary<string, string> Cookies { get; }
        IDictionary<string, string> Headers { get; }
    }
}
