﻿using System.Collections.Generic;

namespace uMod.Common
{
    /// <summary>
    /// Decorator interface
    /// </summary>
    public interface IHookDecorator : IContext
    {
        bool IsRegistered { get; }

        IEnumerable<string> Register();

        void Unregister();
    }
}
