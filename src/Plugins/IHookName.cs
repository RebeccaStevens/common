﻿using System;

namespace uMod.Common
{
    public interface IHookName : IFormattable
    {
        string FullyQualifiedHookName { get; }
        string Name { get; }
        string BaseName { get; }
        string CachedName { get; }
        string EventName { get; }
        string Namespace { get; }
        EventState EventState { get; }

        bool IsEvent { get; }
        bool IsNamespaced { get; }
        bool IsBaseHook { get; }
        bool IsInternal { get; }

        bool Equals(IHookName other);
    }
}
