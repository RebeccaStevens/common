﻿using System.Collections.Generic;

namespace uMod.Common
{
    public interface IPluginProvider
    {
        IPluginConfiguration Configuration { get; }

        IPluginManager Manager { get; }

        IEnumerable<IPlugin> All { get; }
        
        bool Exists(string pluginName);

        IPlugin Get(string pluginName);

        T Get<T>(string pluginName = null) where T : IPlugin;

        IEnumerable<T> Find<T>() where T : IPlugin;

        void LoadAll(bool init = false);

        void UnloadAll(IEnumerable<string> skip = null);

        void ReloadAll(IEnumerable<string> skip = null);

        void Reload(IEnumerable<string> pluginNames);

        void Load(IEnumerable<string> pluginNames);

        void Unload(IEnumerable<string> pluginNames);

        bool Load(string pluginName);

        bool Unload(string pluginName);

        bool Reload(string pluginName);
    }
}
