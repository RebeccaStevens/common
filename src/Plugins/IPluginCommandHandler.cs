﻿using System;
using uMod.Common.Command;

namespace uMod.Common.Plugins
{
    /// <summary>
    /// Represents a plugin command handler
    /// </summary>
    public interface IPluginCommandHandler
    {
        /// <summary>
        /// Add a command
        /// </summary>
        /// <param name="command"></param>
        void Add(ICommandInfo command);

        /// <summary>
        /// Remove a command
        /// </summary>
        /// <param name="command"></param>
        void Remove(ICommandInfo command);

        /// <summary>
        /// Remove a command
        /// </summary>
        /// <param name="command"></param>
        void Remove(string command);

        /// <summary>
        /// Gets command syntax help
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        string GetHelp(string command, string lang = null);

        /// <summary>
        /// Gets command description
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        string GetDescription(string command, string lang = null);

        /// <summary>
        /// Create command callback
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="hook"></param>
        /// <returns></returns>
        CommandCallback CreateCallback(ICommandDefinition definition, string hook);

        /// <summary>
        /// Create command callback
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        CommandCallback CreateCallback(ICommandDefinition definition, Func<IPlayer, IArgs, CommandState> callback);
    }
}
