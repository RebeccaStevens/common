﻿using System;

namespace uMod.Common
{
    /// <summary>
    /// Represents a single log message
    /// </summary>
    public struct LogMessage
    {
        public LogLevel Level;
        public string Message;
        public DateTime Date;
    }
}
