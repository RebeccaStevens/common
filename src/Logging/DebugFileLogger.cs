﻿using System;
using System.IO;

namespace uMod.Common.Logging
{
    /// <summary>
    /// Basic debug single-file logger
    /// </summary>
    public class DebugFileLogger : ILogger
    {
        public LogLevel LogLevel { get; } = LogLevel.Debug;
        public IEvent<ILogger> OnConfigure { get; }
        public IEvent<ILogger> OnAdded { get; }
        public IEvent<ILogger> OnRemoved { get; }
        private readonly string _prefix;
        private readonly string _path;

        /// <summary>
        /// Create a new instance of the DebugFileLogger class
        /// </summary>
        /// <param name="path"></param>
        /// <param name="prefix"></param>
        public DebugFileLogger(string path, string prefix = null)
        {
            _path = path;
            _prefix = prefix;

            string directory = Path.GetDirectoryName(_path)?.Trim();
            if (!string.IsNullOrEmpty(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        /// <summary>
        /// Clear log file
        /// </summary>
        public void Clear()
        {
            FileStream fileStream = File.Open(_path, FileMode.OpenOrCreate);
            fileStream.SetLength(0);
            fileStream.Close();
        }

        /// <summary>
        /// Writes a message to the log file
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public void Write(LogLevel level, string message)
        {
            File.AppendAllText(_path, $"{Environment.NewLine}[{_prefix}] {DateTime.Now:h:mm tt} [{level}] {message}");
        }

        /// <summary>
        /// Writes a message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Write(LogMessage message, string defaultMessage = null)
        {
            File.AppendAllText(_path, $"{Environment.NewLine}[{_prefix}] {message.Date:h:mm tt} [{message.Level}] {message.Message}");
        }

        /// <summary>
        /// Writes a debug message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Debug(string message)
        {
            Write(LogLevel.Debug, message);
        }

        /// <summary>
        /// Writes an informational message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message)
        {
            Write(LogLevel.Info, message);
        }

        /// <summary>
        /// Writes a notice message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Notice(string message)
        {
            Write(LogLevel.Notice, message);
        }

        /// <summary>
        /// Writes a warning message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Warning(string message)
        {
            Write(LogLevel.Warning, message);
        }

        /// <summary>
        /// Writes a error message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            Write(LogLevel.Error, message);
        }

        /// <summary>
        /// Writes a critical message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Critical(string message)
        {
            Write(LogLevel.Critical, message);
        }

        /// <summary>
        /// Writes an alert message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Alert(string message)
        {
            Write(LogLevel.Alert, message);
        }

        /// <summary>
        /// Writes an emergency message to the log file
        /// </summary>
        /// <param name="message"></param>
        public void Emergency(string message)
        {
            Write(LogLevel.Emergency, message);
        }

        /// <summary>
        /// Writes an exception report message to the log file
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Report(string message, Exception exception)
        {
            Exception outerEx = exception;
            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            if (outerEx.GetType() != exception.GetType())
            {
                Write(LogLevel.Error, $"ExType: {outerEx.GetType().Name}");
            }

            Write(LogLevel.Error, $"{message} ({exception.GetType().Name}: {exception.Message}){Environment.NewLine}{exception.StackTrace}");
        }

        /// <summary>
        /// Writes an exception report message to the log file
        /// </summary>
        /// <param name="exception"></param>
        public void Report(Exception exception)
        {
            Exception outerEx = exception;
            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            if (outerEx.GetType() != exception.GetType())
            {
                Write(LogLevel.Error, $"ExType: {outerEx.GetType().Name}");
            }

            Write(LogLevel.Error, $"({exception.GetType().Name}: {exception.Message}){Environment.NewLine}{exception.StackTrace}");
        }
    }
}
