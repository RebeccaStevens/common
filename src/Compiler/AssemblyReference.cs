﻿using System;
using System.IO;

namespace uMod.Common.Compiler
{
    [Serializable]
    public sealed class AssemblyReference
    {
        public string Name { get; set; }
        public string Directory { get; set; }

        public AssemblyReference(string directory, string name)
        {
            Name = name;
            Directory = directory;
        }

        public AssemblyReference(string path)
        {
            Name = Path.GetFileName(path);
            Directory = Path.GetDirectoryName(path);
        }
    }
}
