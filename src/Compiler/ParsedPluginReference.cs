﻿namespace uMod.Common.Compiler
{
    public class ParsedPluginReference
    {
        public string PluginName { get; private set; }
        public bool Optional { get; set; }
        public bool UsingStatement { get { return (ReferenceReasons & PluginReferenceReason.UsingStatment) != 0; } }
        public bool MagicRequires { get { return (ReferenceReasons & PluginReferenceReason.MagicRequires) != 0; } }
        public bool ImplicitReference { get { return (ReferenceReasons & PluginReferenceReason.ImplicitReference) != 0; } }
        public bool ExplicitReference { get { return (ReferenceReasons & PluginReferenceReason.ExplicitReference) != 0; } }

        private PluginReferenceReason ReferenceReasons { get; set; }

        public ParsedPluginReference(string name)
        {
            PluginName = name;
        }

        public void AddReferenceReason(PluginReferenceReason reasons)
        {
            ReferenceReasons |= reasons;
        }
    }

    public enum PluginReferenceReason
    {
        None = 0,
        UsingStatment = 1,
        MagicRequires = 2,
        ImplicitReference = 4,
        ExplicitReference = 8,
    }
}
