﻿using System;

namespace uMod.Common.Compiler
{
    [Serializable]
    public enum CompilationMessageType
    {
        Assembly,
        Compile,
        Error,
        Exit,
        Ready,
        Promise
    }
}
