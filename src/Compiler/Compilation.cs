﻿using System;
using System.Linq;

namespace uMod.Common.Compiler
{
    [Serializable]
    public sealed class Compilation
    {
        public bool LoadDefaultReferences { get; set; }
        public string AssemblyName { get; set; }
        public string OutputFile { get; set; }
        public Platform Platform { get; set; }
        public CompilationFile[] ReferenceFiles { get; set; }
        public AssemblyReference[] AssemblyReferences { get; set; }
        public CompilationFile[] SourceFiles { get; set; }
        public bool StdLib { get; set; }
        public CompilationTarget Target { get; set; }
        public LanguageVersion Version { get; set; }
        public bool Sandbox { get; set; }
        public string[] WhitelistedNamespaces { get; set; }
        public string[] BlacklistedNamespaces { get; set; }
        public string[] Defines { get; set; }
        public string[] Usings { get; set; }

        public Compilation()
        {
            Target = CompilationTarget.DynamicallyLinkedLibrary;
            Platform = Platform.AnyCpu;
            Version = LanguageVersion.Default;
            LoadDefaultReferences = false;
            StdLib = false;
        }

        public bool IsNamespaceBlacklisted(string fullNamespace)
        {
            if (BlacklistedNamespaces == null)
            {
                return false;
            }

            foreach (string namespaceName in BlacklistedNamespaces)
            {
                if (!fullNamespace.StartsWith(namespaceName))
                {
                    continue;
                }

                if (WhitelistedNamespaces != null && WhitelistedNamespaces.Any(fullNamespace.StartsWith))
                {
                    continue;
                }

                return true;
            }
            return false;
        }
    }
}
