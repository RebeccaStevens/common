﻿using System;

namespace uMod.Common.Compiler
{
    [Serializable]
    public class SandboxViolationException : UnauthorizedAccessException
    {
        public SandboxViolationException()
        {
        }

        public SandboxViolationException(string message) : base(message)
        {
        }

        public SandboxViolationException(string message, Exception inner) : base(message, inner)
        {
        }

        protected SandboxViolationException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }
}
