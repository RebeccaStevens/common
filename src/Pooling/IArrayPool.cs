﻿namespace uMod.Common.Pooling
{
    /// <summary>
    /// ArrayPool interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IArrayPool<T>
    {
        T[] Get(int length);

        void Free(T[] array);
    }
}
