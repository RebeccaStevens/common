﻿namespace uMod.Common.Pooling
{
    /// <summary>
    /// Pool interface
    /// </summary>
    public interface IDynamicPool
    {
        T Get<T>();

        void Free<T>(T @object);
    }
}
