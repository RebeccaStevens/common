﻿namespace uMod.Common
{
    public interface IExtension : IResource, IManagedContext<IExtension, IExtensionManager>
    {
        string Branch { get; }
        bool IsCoreExtension { get; }
        bool IsGameExtension { get; }
        bool SupportsReloading { get; }
        string[] DefaultReferences { get; }
        string[] WhitelistedNamespaces { get; }
        string[] BlacklistedNamespaces { get; }

        void Load();

        void Configure();

        void Unload();

        void LoadPluginWatchers(string pluginDirectory);

        void OnModLoad();

        void OnShutdown();

        bool Resolve(string name = null, string fileName = null);
    }
}
