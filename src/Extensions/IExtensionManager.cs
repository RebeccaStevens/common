﻿using System.Collections.Generic;
using System.IO;

namespace uMod.Common
{
    public interface IExtensionManager : ISingleton, IContextManager<IExtension>
    {
        ILogger Logger { get; }

        void RegisterPluginLoader(IPluginLoader loader);

        bool UnregisterPluginLoader(IPluginLoader loader);

        IEnumerable<IPluginLoader> GetPluginLoaders();

        void RegisterLibrary(string name, ILibrary library);

        IEnumerable<string> GetLibraryNames();

        IEnumerable<ILibrary> GetLibraries();

        ILibrary GetLibrary(string name);

        void RegisterChangeWatcher(IChangeWatcher watcher);

        IEnumerable<IChangeWatcher> GetChangeWatchers();

        void RemoveChangeWatcher(IChangeWatcher watcher);

        bool LoadExtension(string filename, bool forced = false);

        bool LoadExtension(IExtension extension, bool forced = false);

        bool UnloadExtension(string filename);

        bool UnloadExtension(IExtension extension);

        bool ReloadExtension(string filename);

        bool ReloadExtension(IExtension extension);

        void LoadFrom(DirectoryInfo directory);

        void LoadFrom(string directory);

        void LoadFrom(string directory, bool coreOnly);

        IEnumerable<IExtension> GetExtensions();

        IExtension GetExtension(string name);

        T GetExtension<T>(string name) where T : class, IExtension;

        bool TryGetExtension(string name, out IExtension extension);

        bool TryGetExtension<T>(string name, out T extension) where T : class, IExtension;

        bool HasExtension(string name);
    }
}
