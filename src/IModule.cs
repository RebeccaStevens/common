﻿using System;
using System.Collections.Generic;

namespace uMod.Common
{
    public interface IModule
    {
        /// <summary>
        /// Gets the root directory
        /// </summary>
        string RootDirectory { get; }

        /// <summary>
        /// Gets the extension directory
        /// </summary>
        string ExtensionDirectory { get; }

        /// <summary>
        /// Gets the instance directory
        /// </summary>
        string InstanceDirectory { get; }

        /// <summary>
        /// Gets the application directory
        /// </summary>
        string AppDirectory { get; }

        /// <summary>
        /// Gets the configuration directory
        /// </summary>
        string ConfigDirectory { get; }

        /// <summary>
        /// Gets the data directory
        /// </summary>
        string DataDirectory { get; }

        /// <summary>
        /// Gets the localization directory
        /// </summary>
        string LangDirectory { get; }

        /// <summary>
        /// Gets the log directory
        /// </summary>
        string LogDirectory { get; }

        /// <summary>
        /// Gets the plugin directory
        /// </summary>
        string PluginDirectory { get; }

        IServer Server { get; }

        GameEngine GameEngine { get; }

        IApplication Application { get; }

        bool IsTesting { get; }

        float Now { get; }

        IEvent OnLoaded { get; }

        IEvent OnLoggingInitialized { get; }

        IPluginProvider Plugins { get; }

        IExtensionProvider Extensions { get; }

        ILibraryProvider Libraries { get; }

        void Load(IInitializationInfo info = null);
        
        object CallHook(string hookname, params object[] args);

        object CallDeprecatedHook(string oldHook, string newHook, DateTime expireDate, params object[] args);

        void NextTick(Action callback);

        void OnFrame(Action<float> callback);

        void OnFrame(float delta);

        void OnSave();

        void OnShutdown();

        void LogDebug(string format, params object[] args);

        void LogError(string format, params object[] args);

        void LogException(string message, Exception ex);

        void LogInfo(string format, params object[] args);

        void LogWarning(string format, params object[] args);
    }
}
