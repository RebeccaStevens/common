﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace uMod.Common
{
    public interface IApplication : IContainer, IImplicitSingletonContainer, IExplicitSingletonContainer, IFactory, ISingleton
    {
        void Resolve(Type type);

        void Resolve(IEnumerable<Type> types);

        void Alias(string name, Type type);

        void AddConverter(Type from, Type to, Func<object, object> conversionFunc = null);

        void AddConverter<T, T1>(Func<T, T1> conversionFunc = null);

        bool Converts(Type from, Type to);

        bool TryGetType(string name, out Type type);

        bool TryGetTypes(string search, out IEnumerable<Type> types);

        bool TryGetObject(Type type, out object @object);

        bool TryGetObject<T>(Type type, out T @object);

        bool TryGetObject<T>(out T @object);

        bool TryGetObject(Type type, out IEnumerable<object> objects);

        bool TryGetObjects<T>(Type type, out IEnumerable<T> objects);

        bool TryGetObjects<T>(out IEnumerable<T> objects);

        bool TryGetSingleton(Type type, out object singleton);

        bool TryGetSingleton<T>(Type type, out T singleton) where T : class;

        bool TryGetSingleton<T>(out T singleton) where T : class;

        TResult Make<TResult>(string alias, object[] arguments = null);

        bool TryInject<T>(object context, object @in, out object @out);

        bool TryInject(Type type, object context, object @in, out object @out);

        IBindingScope When<T>();

        IBindingScope When(params Type[] types);
    }

    public interface IBindingScope
    {
        IList<Type> Context { get; }
        Type NeedsType { get; }
        object BoundObject { get; }
        Func<object, object> BindCallbackWithParameter { get; }
        Func<object> BindCallback { get; }
        MethodInfo BindMethod { get; }
        Type[] BindMethodParameters { get; }

        IBindingScope Needs(Type type);

        IBindingScope Bind(object obj);

        IBindingScope Bind(Func<object, object> callback);

        IBindingScope Bind(Func<object> callback);

        IBindingScope Bind(MethodInfo method);
    }

    public interface IBindingScope<T> : IBindingScope
    {
    }

    /// <summary>
    /// Container interface
    /// </summary>
    public interface IContainer : IContextContainer
    {
        IEnumerable<IContextContainer> GetContainers();

        void Register(IContextContainer container);

        void Unregister(IContextContainer container);

        void Boot();
    }

    /// <summary>
    /// ContextContainer interface
    /// </summary>
    public interface IContextContainer : IImplicitContainer, IExplicitContainer, IDisposable
    {
        IEvent<Type, object> Added { get; }
        IEvent<Type, object> Removed { get; }

        IEvent<Type> AddedType { get; }
        IEvent<Type> RemovedType { get; }

        void Unregister();
    }

    /// <summary>
    /// ImplicitContainer interface
    /// </summary>
    public interface IImplicitContainer
    {
        bool Resolved(Type type);

        bool Resolved(string typeName);

        bool Resolved(object @object);

        bool Resolved(Type type, object @object);

        object Bind(Type type, object @object);

        object Bind(object @object);

        bool Unbind(Type type);

        bool Unbind(Type type, object @object);

        bool Unbind(object @object);
    }

    /// <summary>
    /// ExplicitContainer interface
    /// </summary>
    public interface IExplicitContainer
    {
        bool Resolved<T>();

        T Bind<T>(T @object);

        IEnumerable<T> Bind<T>(IEnumerable<T> objects);

        bool Unbind<T>(IEnumerable<T> objects);

        bool Unbind<T>();

        bool Unbind<T>(T @object);
    }

    /// <summary>
    /// ObjectContainer interface
    /// </summary>
    public interface IObjectContainer : IContextContainer
    {
        bool TryGetObjects(Type type, out IEnumerable<object> objects);

        bool TryGetObjects<T>(Type type, out IEnumerable<T> objects);
    }

    /// <summary>
    /// SingletonContainer interface
    /// </summary>
    public interface ISingletonContainer : IContextContainer
    {
        bool TryGetObject(Type type, out object @object);

        bool TryGetObject<T>(Type type, out T @object);
    }

    public interface IImplicitSingletonContainer : IImplicitContainer
    {
        object BindSingleton(Type type, object @object);

        object BindSingleton(object @object);
    }

    public interface IExplicitSingletonContainer : IExplicitContainer
    {
        T BindSingleton<T>(T @object);
    }
}
