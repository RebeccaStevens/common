﻿using System.Collections.Generic;

namespace uMod.Common
{
    public interface ILibraryProvider
    {
        void Register(string name, ILibrary library);

        IEnumerable<ILibrary> All { get; }

        T Get<T>(string name = null) where T : class, ILibrary;

        ILibrary Get(string name);

        ILibrary this[string name] { get; }
    }
}
